Minimalistic aproach to HSM
===========================

## build
`gcc main.cc --std=c++17 -o NAME` where NAME is out name for program.

## short description

Short approach to HSM implementation.  

To be done better it should:
* have factory instead building std::map manually
* be revritten to use templates for one time instantations.
* use any logger class instead `std::cout` - std::cout rather shows were logs should be i.e. for final version for states debugging.

## Sum up

In real life rather than writting my own, I would rather check some library to do so.
I.e. there is new aproach in boost [SML](https://boost-experimental.github.io/sml/) which looks promissing, especially in comparison to `boost MSM`
