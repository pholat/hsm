#include <iostream>
#include <memory>
#include <functional>
#include <map>
#include <variant>
#include <utility>

// TODO
// * add templates - to staticly check evts, transition pairs
// * use std::cout/std::cerr in debug only to dump data only if desirable

namespace {
    using namespace std;

    class Ctx {
        public:
            Ctx() {};
    };

    enum Evt {
        None,
        e0,
        e1,
        e2,
        e3
    };

    class TransEx : public exception {
        public:
            TransEx() {};
    };

    typedef function<int(Ctx*)> Runner;
    class Transition;

    struct BaseState {
        enum St {
            None,
            S1,
            S2,
            S3,
        };
        St st_in, st_out;
        std::map<St, Transition> *trans_table;
        virtual BaseState& run(Evt e, Ctx *ctx) = 0;
    };

    struct State : public BaseState {
        State(St st_in=St::None, St st_out=St::None, std::map<St,Transition>*trans_table=NULL);
        virtual BaseState& run(Evt e, Ctx *ctx);
    };

    class Parent {
        variant<shared_ptr<Transition>,Transition*> p_ptr;

        public:
        const bool exists;
        Parent() : exists(false) {}
        Parent(variant<shared_ptr<Transition>,Transition*> p_ptr) : p_ptr(p_ptr), exists(true) {}
        Transition* get_transition() {
            if (holds_alternative<Transition*>(p_ptr)) {
                return get<Transition*>(p_ptr);
            } else {
                return get<shared_ptr<Transition>>(p_ptr).get();
            }
        }
    };

    class Transition {
        // we may want to share ptr to dymamic or static parent...
        Parent parent;
        Evt my_evt;
        State ret;
        Runner r;

        public:
        Transition(Evt e=Evt::e0, State &&ret=State(), Runner r=[](Ctx*){return 1;}, Parent parent = {}) :parent(parent), my_evt(e), ret(ret)
        {
            this->r=r;
        }

        State &get_state()
        {
            return ret;
        }

        Transition* get_parent()
        {
            if (parent.exists) {
                return parent.get_transition();
            } else {
                return NULL;
            }
        }

        BaseState &run(Evt e, Ctx* ctx)
        {
            /// check if evt isnt ours
            if(e == this->my_evt) {
                std::cout << "On event " << my_evt << " run result: " << r(ctx) << std::endl;
                return ret;
            /// check if there is parent -> & check with parents
            } else if(get_parent() != NULL) {
                return get_parent()->run(e,ctx);
            /// else either ignore or throw - we can throw so why not
            } else {
                std::cout << "Cant handle, no parents -> let's die" << std::endl;
                throw TransEx();
            }
        }
    };


    State::State(St st_in,
                 St st_out,
                 std::map<St,Transition>*trans_table) {
        this->st_in=st_in;
        this->st_out=st_out;
        this->trans_table=trans_table;
    }

    BaseState& State::run(Evt e, Ctx *ctx)
    {
        return trans_table->at(st_in).run(e,ctx);
    }
}

// stupid simple aproach
void run_hsm() {
    Ctx ctx;

    std::map<State::St, Transition> root_st_map {
        {   // on S1
            State::St::S1,
            // Perform Transition for e0 to def S2, with action and none parent
            Transition(e0,
                       State(State::St::S1, State::St::S2, &root_st_map),
                       [](Ctx *ctx){ return State::St::S1;}
                       )
        },
        {   // on S2
            State::St::S2,
            // Perform Transition to e1 def S1, with action and none parent
            Transition(e1,
                       State(State::St::S2, State::St::S1, &root_st_map),
                       [](Ctx *ctx){ return State::St::S2;}
                       )
        },
    };
    std::map<State::St, Transition> derived_st_map {
        {   // on S3
            State::St::S3,
            // Perform Transition for e0 to def S2, with action and none parent
            Transition(e3,
                       State(State::St::S3, State::St::None, &derived_st_map),
                       [](Ctx *ctx){ return State::St::S3;},
                       Parent(&root_st_map[State::St::S1])
                       )
        },
    };

    /// this is our pipe getter
    /// as it gets messages of constant size, for simplicity, I assume that it just sends next evt id in enum element size
    std::function<Evt()> get_evt = [](){
        static int pos=0;
        Evt t[] = {
            // run with parent
            e0,
            // run with evt in same scope
            e1,
            // run with evt out of scope
            e1,
        };
        return t[pos++];
    };

    /// run from derived state -> next state == State from parent
    Evt e=get_evt();
    // we need to start somewhere, this is our root
    BaseState &st = derived_st_map[State::St::S3].get_state();
    // we need to find point of start
    try {
        do {
            std::cout << "With evt: " << e << " run st: " << st.st_in << std::endl;
            st = st.run(e, &ctx);
        } while((e=get_evt())!=Evt::None);
    } catch(TransEx ex) {
        std::cout << "As designed - caught incompatible evt handling" << std::endl;
    }
}

int main(int argc, char *argv[])
{
    run_hsm();
}
